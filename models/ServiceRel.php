<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "service_rel".
 *
 * @property integer $parent_id
 * @property string $child_id
 *
 * @property Service $parent
 */
class ServiceRel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_rel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['child_id'], 'required'],
            [['child_id'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Service::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parent_id' => 'Parent ID',
            'child_id' => 'Child ID',
        ];
    }

    public function getChildItem()
    {
        return  $this->hasOne(Service::className(), ['id' => 'child_id']);
    }

    public function removeRelation($id){
        $data = $this->find()->where(['parent_id'=>$id])->all();

        foreach ($data as $item){
            $item->delete();
        }
    }

}
