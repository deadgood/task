<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "service".
 *
 * @property integer $id
 * @property string $title
 *
 * @property ServiceRel $serviceRel
 */
class Service extends \yii\db\ActiveRecord
{
    public $services;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['services'], 'safe'],
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'services' => 'Services'
        ];
    }

    public function getChild()
    {
        return  $this->hasMany(ServiceRel::className(), ['parent_id' => 'id']);
    }


    public function saveRel($post)
    {
        if (!empty($post['Service']['services'])) {

            foreach ($post['Service']['services'] as $service) {
                $rel = new ServiceRel();
                $rel->parent_id = $this->id;
                $rel->child_id = $service;
                $rel->save();
            }

        }
    }

}
