<?php

use yii\db\Migration;

/**
 * Class m171122_185431_add_primary_to_service_rel
 */
class m171122_185431_add_primary_to_service_rel extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('service_rel', 'id', $this->primaryKey());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('service_rel', 'id');
    }
}
