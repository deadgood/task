<?php

use yii\db\Migration;

/**
 * Class m171122_121111_service_rel
 */
class m171122_121111_service_rel extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('service_rel', [
            'parent_id' => $this->integer()->notNull(),
            'child_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('service_rel');
    }

}
