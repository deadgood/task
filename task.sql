/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50720
Source Host           : localhost:3306
Source Database       : task

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2017-11-23 10:06:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth_assignment
-- ----------------------------
DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `auth_assignment_user_id_idx` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auth_assignment
-- ----------------------------
INSERT INTO `auth_assignment` VALUES ('adminAccess', '5', '1511422275');
INSERT INTO `auth_assignment` VALUES ('adminAccess', '6', '1511422469');
INSERT INTO `auth_assignment` VALUES ('giiAccess', '5', '1511422969');
INSERT INTO `auth_assignment` VALUES ('rbacAccess', '5', '1511422277');
INSERT INTO `auth_assignment` VALUES ('serviceAccess', '5', '1511422279');
INSERT INTO `auth_assignment` VALUES ('serviceAccess', '6', '1511422309');
INSERT INTO `auth_assignment` VALUES ('updateServices', '5', '1511423810');
INSERT INTO `auth_assignment` VALUES ('updateServices', '6', '1511423640');
INSERT INTO `auth_assignment` VALUES ('userAccess', '4', '1511422265');
INSERT INTO `auth_assignment` VALUES ('userAccess', '5', '1511422278');
INSERT INTO `auth_assignment` VALUES ('userAccess', '6', '1511422474');
INSERT INTO `auth_assignment` VALUES ('userAccess', '7', '1511422320');
INSERT INTO `auth_assignment` VALUES ('viewServices', '5', '1511423813');

-- ----------------------------
-- Table structure for auth_item
-- ----------------------------
DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auth_item
-- ----------------------------
INSERT INTO `auth_item` VALUES ('/admin/*', '2', null, null, null, '1511359934', '1511359934');
INSERT INTO `auth_item` VALUES ('/admin/service/*', '2', null, null, null, '1511359984', '1511359984');
INSERT INTO `auth_item` VALUES ('/gii/*', '2', null, null, null, '1511422907', '1511422907');
INSERT INTO `auth_item` VALUES ('/rbac/*', '2', null, null, null, '1511359950', '1511359950');
INSERT INTO `auth_item` VALUES ('/site/*', '2', null, null, null, '1511422176', '1511422176');
INSERT INTO `auth_item` VALUES ('adminAccess', '1', 'admine rights', null, null, '1511422108', '1511422933');
INSERT INTO `auth_item` VALUES ('giiAccess', '1', 'access gii', null, null, '1511422964', '1511423012');
INSERT INTO `auth_item` VALUES ('rbacAccess', '1', 'acess to roles', null, null, '1511422081', '1511422081');
INSERT INTO `auth_item` VALUES ('serviceAccess', '1', 'access to service', null, null, '1511422007', '1511422032');
INSERT INTO `auth_item` VALUES ('updateServices', '2', 'update services', null, null, '1511423576', '1511423576');
INSERT INTO `auth_item` VALUES ('userAccess', '1', 'simple user access', null, null, '1511422156', '1511422184');
INSERT INTO `auth_item` VALUES ('viewServices', '2', 'view services', null, null, '1511423682', '1511423682');

-- ----------------------------
-- Table structure for auth_item_child
-- ----------------------------
DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auth_item_child
-- ----------------------------
INSERT INTO `auth_item_child` VALUES ('adminAccess', '/admin/*');
INSERT INTO `auth_item_child` VALUES ('serviceAccess', '/admin/service/*');
INSERT INTO `auth_item_child` VALUES ('updateServices', '/admin/service/*');
INSERT INTO `auth_item_child` VALUES ('giiAccess', '/gii/*');
INSERT INTO `auth_item_child` VALUES ('rbacAccess', '/rbac/*');
INSERT INTO `auth_item_child` VALUES ('userAccess', '/site/*');

-- ----------------------------
-- Table structure for auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auth_rule
-- ----------------------------

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` blob,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------

-- ----------------------------
-- Table structure for migration
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of migration
-- ----------------------------
INSERT INTO `migration` VALUES ('m000000_000000_base', '1511352974');
INSERT INTO `migration` VALUES ('m140506_102106_rbac_init', '1511359806');
INSERT INTO `migration` VALUES ('m140602_111327_create_menu_table', '1511357323');
INSERT INTO `migration` VALUES ('m160312_050000_create_user', '1511357323');
INSERT INTO `migration` VALUES ('m170907_052038_rbac_add_index_on_auth_assignment_user_id', '1511359806');
INSERT INTO `migration` VALUES ('m171122_114253_service', '1511353782');
INSERT INTO `migration` VALUES ('m171122_121111_service_rel', '1511353782');
INSERT INTO `migration` VALUES ('m171122_185431_add_primary_to_service_rel', '1511421776');

-- ----------------------------
-- Table structure for service
-- ----------------------------
DROP TABLE IF EXISTS `service`;
CREATE TABLE `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of service
-- ----------------------------
INSERT INTO `service` VALUES ('12', 'tstsat');
INSERT INTO `service` VALUES ('13', 'service2');
INSERT INTO `service` VALUES ('14', 'service3');

-- ----------------------------
-- Table structure for service_rel
-- ----------------------------
DROP TABLE IF EXISTS `service_rel`;
CREATE TABLE `service_rel` (
  `parent_id` int(11) NOT NULL,
  `child_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of service_rel
-- ----------------------------
INSERT INTO `service_rel` VALUES ('14', '12', '5');
INSERT INTO `service_rel` VALUES ('14', '13', '6');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('4', 'user', 'HFQ_KvqvE0yqRnb6DHXIlrJ6uk1IpELp', '$2y$13$SVuQ1tc4ZPQWTkd/IL3HfOETjzzRVjHXJ.a64KRGA6oSgENrVMAm6', null, 'user@gmail.com', '10', '1511359727', '1511359727');
INSERT INTO `user` VALUES ('5', 'admin', 'oCu7Yu9hx6anpKdly6hN5ybCaCZojeH1', '$2y$13$hBv4DTxyzq5oLLLsdz3t..qPZEtgc1aJSRCtwwN9F3Gw/noRcZTfW', null, 'admin@gmail.com', '10', '1511359856', '1511359856');
INSERT INTO `user` VALUES ('6', 'manager', 'rAVlNT8zVywvbwuvfrb67RxotdjhwcN-', '$2y$13$Jxx4VgPI9Ad6PgK5pxTbl.o1GRTkdTvXIbmhArv2X0Di9woDv7mUa', null, 'manager@gmail.com', '10', '1511359883', '1511359883');
INSERT INTO `user` VALUES ('7', 'test', 'ACUT6CmE06S4mJT5LigCC5k9dreGaK56', '$2y$13$lcVV7U.Vf9h6Ql32gsd3YeIbfC2Y3A.f1kQAjhZkr7utbCd6m6RPC', null, 'test@gmail.com', '10', '1511361286', '1511361286');
