<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Service */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete',
            ['delete', 'id' => $model->id],
            [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            [
                'label' => 'Child services',
                'format'=>'raw',
                'value' => function ($model) {
                    $return = '';
                    if (!empty($model->child)) {
                        $i = 0;
                        foreach ($model->child as $item) {
                            $itemData = $item->getChildItem()->one();
                            if ($i == 0) {
                                $return = Html::a($itemData->title, ['/admin/service/view?id='.$itemData->id,], []);
                            } else {
                                $return .=  Html::a((' ,' . $itemData->title), ['/admin/service/view?id='.$itemData->id,], []);
                            }
                            $i++;
                        }

                    } else {
                        $return = 'None';
                    }
                    return $return;
                }

            ],
        ],
    ]) ?>

</div>
