<?php

namespace app\modules\admin\controllers;

use app\models\ServiceRel;
use Yii;
use app\models\Service;
use app\models\ServiceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * ServiceController implements the CRUD actions for Service model.
 */
class ServiceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Service models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ServiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
    }

    /**
     * Displays a single Service model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {

        if(!Yii::$app->user->can('viewServices')){
            return $this->redirect(['/admin/service']);
        }
        $model = Service::find()->where(['id'=>$id])->with('child')->one();

        return $this->render('view', compact('model'));
    }

    /**
     * Creates a new Service model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Service();

        $services = ArrayHelper::map(Service::find()->all(), 'id', 'title');

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $model->save();
            $model->saveRel(Yii::$app->request->post());

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', compact('model', 'services'));
        }
    }

    /**
     * Updates an existing Service model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(!Yii::$app->user->can('updateServices')){
            return $this->redirect(['/admin/service']);
        }

        $model = Service::find()->where(['id'=>$id])->with('child')->one();
        $selected = [];
        if (!empty($model->child)) {
            foreach ($model->child as $item) {
                $selected[] = $item->child_id;
            }
            $model->services = $selected;
        }
        $services = ArrayHelper::map(Service::find()->where(['<>', 'id', $id])->all(), 'id', 'title');

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $model->save();
            $relModel = new ServiceRel();
            $relModel->removeRelation($model->id);
            $model->saveRel(Yii::$app->request->post());
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', compact('services', 'model'));
        }
    }

    /**
     * Deletes an existing Service model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = Service::findOne($id);
        $model->delete();
        $relModel = new ServiceRel();
        $relModel->removeRelation($id);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Service model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Service the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Service::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
